package HomeSecurityController;


// Standard Java Packages
import java.util.Date;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.HashSet;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.ConcurrentHashMap;

// RMI packages
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

// IoT Runtime Packages
import iotruntime.slave.IoTSet;
import iotruntime.slave.IoTRelation;
import iotcode.annotation.*;

// IoT Driver Packages
import iotcode.interfaces.*;

// Checker annotations
//import iotchecker.qual.*;

// Using FIDELIUS system
import iotcloud.*;

/** Class Home Security Controller for the home security application benchmark
 *
 * @author      Rahmadi Trimananda <rtrimana @ uci.edu>
 * @version     1.0
 * @since       2016-12-14
 */
public class HomeSecurityController implements SmartthingsSensorCallback {

	/*
	 *  Constants
	 */
	private static final int MOTION_TIME_THRESHOLD = 60; 	// in seconds
	private static final int CAMERA_FPS = 15;
	private static final int CHECK_TIME_WAIT = 1;			// in seconds
	private static final int SECOND_TO_TURN_ON = 60;		// in seconds
	private static final int SECOND_TO_TURN_OFF = 1;		// in seconds
    
    private static final int CAM_NUM = 2;
    private static final int SEN_NUM = 3; 
    private static final int ROOM_NUM = 2;
    


    // Made by Dohyun 
    /*
     *  for iotcloud
     */
    Table t1 = null;
    String str[] = new String[10];
    IoTString istr[] = new IoTString[10];

    // for key 
    IoTString icam_detect_num[] = new IoTString[CAM_NUM];   //check whether camera detect something or not
    IoTString isen_detect_num[] = new IoTString[SEN_NUM];   //check whether sensors detect somthing or not
    IoTString icam_able_num[] = new IoTString[CAM_NUM];     // enable / disable camera 
    IoTString isen_able_num[] = new IoTString[SEN_NUM];     // enable / disable sensor
    IoTString ialarm_num[] = new IoTString[ROOM_NUM];       // on / off alarm
    IoTString iall_alarm = new IoTString("all_alarm");      // enable / disable alarm
    
    //for value
    IoTString iable = new IoTString("able");               
    IoTString idisable = new IoTString("disable");
    IoTString ion = new IoTString("on");
    IoTString ioff = new IoTString("off");
    IoTString itrue = new IoTString("true");
    IoTString ifalse = new IoTString("false");


	/**
	 *  IoT Sets and Relations
	 *  <p>
	 *  Devices involved:
	 *  1) Multipurpose sensor (detect windows open/close) - Smartthings sensor
	 *  2) Motion sensor (detect motion in certain radius) - Smartthings sensor
	 *  3) Water leak sensor (detect leakage) - Smartthings sensor
	 *  4) Camera (detect motion)
	 *  5) Alarm (using ESP board) - assuming 1 house alarm
	 *  6) Room (object as place of device)
	 *
	 *  Additionals (for more extensive home management)
	 *  7) Doorlock (detect open/locked)
	 *  8) Power outlet (detect on/off, monitor watt)
	 */
	// This comprises multipurpose, motion, and water leak sensors
	// TODO: Per 01/2017, doorlock and outlet are not ready, ESP board will be used for alarm
	@config private IoTSet<SmartthingsSensorSmart> smartSensorsSet;
	@config private IoTSet<CameraSmart> camSet;
	@config private IoTSet<AlarmSmart> alarmSet;
	@config private IoTSet<RoomSmart> roomSet;
	@config private IoTSet<DoorLock> doorlockSet;
	//@config private IoTSet<Outlet> outletSet;

	@config private IoTRelation<RoomSmart, SmartthingsSensorSmart> roomSensorRelation;
	@config private IoTRelation<RoomSmart, CameraSmart> roomCameraRelation;
	@config private IoTRelation<RoomSmart, DoorLock> roomDoorLockRelation;
	//@config private IoTRelation<RoomSmart, Outlet> roomOutletRelation;

	/*******************************************************************************************************************************************
	**
	**  Variables
	**
	*******************************************************************************************************************************************/
	long lastTimeChecked = 0;

	private static int sensorId = 0;

	/*******************************************************************************************************************************************
	**
	**  States data structures
	**
	*******************************************************************************************************************************************/
	// Camera and motion detection
	private Map<CameraSmart, MotionDetection> camMotionDetect =
		new HashMap<CameraSmart, MotionDetection>();
	// Smartthings sensors (true = motion, leakage, etc.)
	private Map<Integer, Boolean> senDetectStatus =
		new ConcurrentHashMap<Integer, Boolean>();
	// Camera (true = motion)
	private Map<CameraSmart, Boolean> camDetectStatus =
		new HashMap<CameraSmart, Boolean>();
	// Doorlock (true = open - not locked)
    private Map<DoorLock, Boolean> doorlockStatus =
	    new HashMap<DoorLock, Boolean>();
	// Outlet (true = on - outlet is used)
	//private Map<Outlet, Boolean> outletStatus =
	//	new HashMap<Outlet, Boolean>();

	// Alarm status
	private Map<Integer, Boolean> alarmStatus =
		new HashMap<Integer, Boolean>();

	public HomeSecurityController() {

	}


	/*******************************************************************************************************************************************
	**
	**  Helper Methods
	**
	*******************************************************************************************************************************************/


	/** Method to initialize Smartthings sensors
	 *
	 *   @return [void] None.
	 */
	private void initSmartthingsSensors(RoomSmart rm) {

		// Get and init the IAS sensors for this specific room
		HashSet<SmartthingsSensorSmart> sensors = roomSensorRelation.get(rm);
		System.out.println("DEBUG: We have " + sensors.size() + " sensors!");
		for (SmartthingsSensorSmart sen : sensors) {
	
			try {
				// Initialize sensors
				sen.init();
				System.out.println("DEBUG: Initialized smartthings sensor! ID: " + sensorId + " Room ID: " + rm.getRoomID());
				senDetectStatus.put(sensorId, false);
				System.out.println("DEBUG: Initialized sensor detection to false!");
				sen.setId(sensorId++);
				sen.registerCallback(this);
				System.out.println("DEBUG: Registered sensor callback!");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}


	/** Method to initialize cameras
	 *
	 *   @return [void] None.
	 */
	private void initCameras(RoomSmart rm) {

		// Get and init the IAS sensors for this specific room
		HashSet<CameraSmart> cameras = roomCameraRelation.get(rm);
		// Setup the cameras, start them all and assign each one a motion detector
		for (CameraSmart cam : cameras) {

			// Each camera will have a motion detector unique to it since the motion detection has state
			MotionDetection mo = new MotionDetection(12, 0.5f, 10, 10);

			// initialize the camera, might need to setup some stuff internally
			cam.init();

			// set the camera parameters.
			cam.setFPS(CAMERA_FPS);
			cam.setResolution(Resolution.RES_VGA);

			// camera will call the motion detector directly with data not this controller
			cam.registerCallback(mo);

			// Start the camera (example is start the HTTP stream if it is a network camera)
			cam.start();
			System.out.println("DEBUG: Initialized camera!");

			// Remember which motion detector is for what camera
			camMotionDetect.put(cam, mo);

			// Initialize detection to false
			camDetectStatus.put(cam, false);
           
		}
	}


	/** Method to initialize alarms
	 *
	 *   @return [void] None.
	 */
	private void initAlarms() {

		// Get and init the alarm (this single alarm set can serve multiple zones / rooms)
		Iterator alarmIt = alarmSet.iterator();
		AlarmSmart alm = (AlarmSmart) alarmIt.next();
		// init the alarm controller, do it here since it only needs to be done once per controller
		try {
			alm.init();
			System.out.println("DEBUG: Initialized alarm!");
			// TODO: Check that this initialization works for multiple times - might be that setZone() only works once!
			for (RoomSmart room : roomSet.values()) {
				turnOffAlarms(room.getRoomID());
				System.out.println("DEBUG: Initialized alarm for room (turn off): " + room.getRoomID());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	/** Method to initialize doorlocks
	 *
	 *   @return [void] None.
	 */
	private void initDoorLocks(RoomSmart rm) {

		//Get and init the doorlocks for this specific room
		HashSet<DoorLock> doorlocks = roomDoorLockRelation.get(rm);
		for (DoorLock doorlock : doorlocks) {
	
			try {
				// Initialize doorlocks
				doorlock.init();
				System.out.println("DEBUG: Initialized doorlock!");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}


	/** Method to initialize power outlets
	 *
	 *   @return [void] None.
	 */
	private void initOutlets(RoomSmart rm) {

		// Get and init the outlets for this specific room
		/*HashSet<Outlet> outlets = roomOutletRelation.get(rm);
		for (Outlet outlet : outlets) {
	
			try {
				// Initialize outlets
				outlet.init();
				System.out.println("DEBUG: Initialized outlet!");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}*/
	}


	/** Method to detect if a room has seen motion within the last few seconds (time specified as parameter).
	 *   Checks all the motion detectors for the given room
	 *
	 *   @param _room            [RoomSmart] , Room of interest.
	 *   @param _numberOfSeconds [int]  , Number of seconds in the past that we consider recent.
	 *   @param _upperThreshold  [int]  , Number of seconds as an upper bound before we turn off.
	 *
	 *   @return [boolean] None.
	 */
	private boolean roomDidHaveMotionRecently(RoomSmart _room, int _numberOfSeconds) {
		long currentTimeSeconds = (new Date()).getTime() / 1000;

		// Loop through all the cameras in the room
		for (CameraSmart cam : roomCameraRelation.get(_room)) {
			long lastDetectedMotionSeconds = currentTimeSeconds;

			Date motionTime = ((MotionDetection)camMotionDetect.get(cam)).getTimestampOfLastMotion();

			// Motion was detected at least once
			if (motionTime != null) {
				lastDetectedMotionSeconds = motionTime.getTime() / 1000;
			} else {
				// motionTime == null means this is the initialization phase
				// so we put false
				return false;
			}

			// Did detect motion recently
			if (Math.abs(currentTimeSeconds - lastDetectedMotionSeconds) < _numberOfSeconds) {
				return true;
			}
		}

		return false;
	}


	/** Method to update state data structures for Smartthings sensors
	 *
	 *   @return [void] None.
	 */
	public void newReadingAvailable(int _sensorId, int _value, boolean _activeValue) {

		System.out.println("DEBUG: Sensor reading value: " + _value);
		if(_activeValue) {
			System.out.println("DEBUG: Sensor is detecting something: " + _activeValue);
			senDetectStatus.put(_sensorId, true);
            TableAddKV(t1,isen_detect_num[_sensorId],itrue);

		} else {
			//System.out.println("DEBUG: Sensor is not detecting something: " + _activeValue);
			senDetectStatus.put(_sensorId, false);
            TableAddKV(t1,isen_detect_num[_sensorId],ifalse);
		} 
	}


	/** Method to update state data structures for doorlocks
	 *
	 *   @return [void] None.
	 */
	private void updateDoorLockStatus(RoomSmart rm) {

		// Get and init the outlets for this specific room
		HashSet<DoorLock> doorlocks = roomDoorLockRelation.get(rm);
		for (DoorLock doorlock : doorlocks) {
	
			// Change is detected! Set to true for report...
//			if(doorlock.isChangeDetected()) {

            if(true){
				doorlock.UnlockDoor();
			} else {

				doorlockStatus.put(doorlock, false);
			}
		}
	}


	/** Method to update state data structures for outlets
	 *
	 *   @return [void] None.
	 */
	private void updateOutletStatus(RoomSmart rm) {

		// Get and init the outlets for this specific room
		/*HashSet<Outlet> outlets = roomOutletRelation.get(rm);
		for (Outlet outlet : outlets) {
	
			// Change is detected! Set to true for report...
			if(isChangeDetected()) {

				outletStatus.put(outlet, true);
			} else {

				outletStatus.put(outlet, false);
			}
		}*/
	}


	/** Update the status of all devices
	 *
	 *   @return [void] None.
	 */
	private void updateUniversalStatus() {

		// Check for motion in rooms and if there is motion then report
		for (RoomSmart room : roomSet.values()) {

			// Update status of camera
			updateCameraStatus(room);
            
            //Update status of sensors.
            //Made by DoHyun
            //updateSmartthingSensorStatus(room);

			// Update status of doorlocks
			updateDoorLockStatus(room);

			// Update status of outlets
			//updateOutletStatus(room);
		}
	}


	/** Update the status of all devices
	 *
	 *   @return [void] None.
	 */
	private void updateCameraStatus(RoomSmart room) {

		HashSet<CameraSmart> cameras = roomCameraRelation.get(room);
		if (roomDidHaveMotionRecently(room, MOTION_TIME_THRESHOLD)) {

			// Motion was detected
			System.out.println("DEBUG: Camera detected something!");
			for(CameraSmart cam : cameras)
				camDetectStatus.put(cam, true);
                TableAddKV(t1,icam_detect_num[room.getRoomID()],itrue);
		} else {

			// No motion was detected
			//System.out.println("DEBUG: Camera didn't detect anything!");
			for(CameraSmart cam : cameras){
				camDetectStatus.put(cam, false);
                TableAddKV(t1,icam_detect_num[room.getRoomID()],itrue);
	    	}
        }
	}
    

   
    //Made by DoHyun
    /** Update the Status of all devices 
      *
      * @return [void] None.
      *
      */
   /* private void updateSmartthingSensorStatus(RoomSmart room){
        
        for (SmartthingsSensorSmart sensor : roomSensorRelation.get(room)){
        
            if(sensor.isActiveValue()){
                // Sensor detected something
                System.out.println("DEBUG: Sensor detected somthing!");
                senDetectStatus.put(sensor.getId(),true);
                TableAddKV(t1,isen_detect_num[sensor.getId()],itrue);
            }
            
            else{
                senDetectStatus.put(sensor.getId(),false);
                TableAddKV(t1,isen_detect_num[sensor.getId()],ifalse);
            }
        }
    }
    */

	/** Method to turn on alarms
	 *
	 *   @return [void] None.
	 */
	private void turnOnAlarms(int zoneId) {

		// Get and init the alarm (this single alarm set can serve multiple zones / rooms)
		Iterator alarmIt = alarmSet.iterator();
		AlarmSmart alm = (AlarmSmart) alarmIt.next();
		alm.setZone(zoneId, true, SECOND_TO_TURN_OFF);
	}


	/** Method to turn off alarms
	 *
	 *   @return [void] None.
	 */
	private void turnOffAlarms(int zoneId) {

		// Get and init the alarm (this single alarm set can serve multiple zones / rooms)
		Iterator alarmIt = alarmSet.iterator();
		AlarmSmart alm = (AlarmSmart) alarmIt.next();
		// Turn this alarm off indefinitely
		alm.setZone(zoneId, false, SECOND_TO_TURN_ON);
	}

    private void TableAddKV(Table t,IoTString key, IoTString value){
        t.update();
        t.startTransaction();
        t.addKV(key,value);
        t.commitTransaction();
    }

	/** Check status of devices and turn on alarm accordingly
	 *  <p>
	 *  Simple rule is whenever any sensor or camera detect something unusual
	 *	(sensor/camera becomes active) then we sound the corresponding alarm.
	 *  This means we send the signal to the right zone in the EspAlarm
	 *
	 *   @return [void] None.
	 */
	private void decideToTurnOnAlarm() {

		int zoneId = 0;

		// Check for motion in rooms and if there is motion then report
		for (RoomSmart room : roomSet.values()) {

			// Loop through all the cameras in the room
			for (CameraSmart cam : roomCameraRelation.get(room)) {

				// Get the right camera and the right detection status (true or false)
                
                t1.update();
				zoneId = room.getRoomID();
                IoTString icam_val = t1.getCommitted(icam_able_num[zoneId]);
                if ((icam_val!=null)&&(icam_val.equals(iable))&&(camDetectStatus.get(cam))) {
					turnOnAlarms(zoneId);
                    TableAddKV(t1, ialarm_num[zoneId] , ion);
					System.out.println("DETECTION: Camera active in room: " + zoneId);
				}else{
                    System.out.println("NOTHING DETECTION cam"+room.getRoomID());
                }
			}

			// Loop through all the cameras in the room
			for (SmartthingsSensorSmart sensor : roomSensorRelation.get(room)) {

				// Get the right sensor and the right detection status (true or false)
				//System.out.println("ABOUT TO DETECT: Reading sensor: " + sensor.getId());
				t1.update();
                IoTString isen_val = t1.getCommitted(isen_able_num[sensor.getId()]);
                if ((isen_val!=null)&&(isen_val.equals(iable))&&(senDetectStatus.get(sensor.getId()))) {
					zoneId = room.getRoomID();
					turnOnAlarms(zoneId);
                    TableAddKV(t1, ialarm_num[zoneId] , ion);
					System.out.println("DETECTION: Sensor active in room: " + zoneId);
					System.out.println("DETECTION: Detection by sensor: " + sensor.getId());
				}else{
                    System.out.println("NOTHING DETECTION sen"+sensor.getId());
                }
			}
		}
	}


	/** Check status of devices and turn off alarm accordingly
	 *  <p>
	 *  If everything (camera and sensors) is set back to normal
	 *  then the system will turn off the alarm
	 *
	 *   @return [void] None.
	 */
    // TODO: Need to fix this part later
	// TODO: Perhaps we should use a phone app to turn off the alarm
	/*private void decideToTurnOffAlarm() {

		// Check for motion in rooms and if there is motion then report
		for (RoomSmart room : roomSet.values()) {

			int zoneId = room.getRoomID();
			// Loop through all the cameras in the room
			for (CameraSmart cam : roomCameraRelation.get(room)) {

				// Get the right camera and the right detection status (true or false)
				if (camDetectStatus.get(cam))	// Camera still active so alarm is still on, so false for off-alarm status

					alarmStatus.put(zoneId, false);

				else

					alarmStatus.put(zoneId, true);
				
			}

			// Loop through all the cameras in the room
			for (SmartthingsSensor sensor : roomSensorRelation.get(room)) {

				// Get the right sensor and the right detection status (true or false)
				if (senDetectStatus.get(sensor.getId()))	// Sensor still active so alarm is still on, so false for off-alarm status

					alarmStatus.put(zoneId, false);

				else

					alarmStatus.put(zoneId, true);
			}
		}

		// Turn on alarm in the right zone
		for (Map.Entry<Integer, Boolean> alEnt : alarmStatus.entrySet()) {
			if (alEnt.getValue())	// if this zone is true, that means we need to turn off its alarm
                turnOffAlarms(alEnt.getKey());

		}
	}*/


    public void ready() {

        try {
            // making Table
            t1 = new Table("http://dc-6.calit2.uci.edu/test.iotcloud/", "reallysecret", 397, -1);

            t1.rebuild();
            // value of each keys initialization
            t1.update();
            t1.startTransaction();
            t1.addKV(iall_alarm, iable);
            for(int i = 0 ; i < CAM_NUM; i++) {              
                 icam_detect_num[i] = new IoTString("cam_detect"+Integer.toString(i));
                 icam_able_num[i] = new IoTString("cam_able"+Integer.toString(i));
                 t1.addKV(icam_detect_num[i],ifalse);
                 t1.addKV(icam_able_num[i],iable);
            }      
             for(int i = 0 ; i < SEN_NUM; i++) {              
                isen_detect_num[i] = new IoTString("sen_detect"+Integer.toString(i));
                isen_able_num[i] = new IoTString("sen_able"+Integer.toString(i));
                t1.addKV(isen_detect_num[i],ifalse);
                t1.addKV(isen_able_num[i],iable);
            }      
            for(int i = 0 ; i < ROOM_NUM; i++) {              
                ialarm_num[i] = new IoTString("alarm"+Integer.toString(i));
                t1.addKV(ialarm_num[i],ioff);
            }      
            
            t1.commitTransaction();

        } catch(Exception e) {
            e.printStackTrace();
        } finally {
        }
    }

	/*******************************************************************************************************************************************
	**
	**  Public Methods
	**
	*******************************************************************************************************************************************/

	/** Initialization method, called by the runtime (effectively the main of the controller)
	 *   This method runs a continuous loop and is blocking
	 *
	 *   @return [void] None;
	 */
	public void init() {

		// Iterate over the set of rooms
		for (RoomSmart rm : roomSet.values()) {

			// Init all Smartthings sensors
			initSmartthingsSensors(rm);

			// Init all cameras
            initCameras(rm);

			// Init all doorlocks
			initDoorLocks(rm);

			// Init all outlets
			//initOutlets()
		}

		// Init all alarms
		initAlarms();
		System.out.println("DEBUG: Initialized all devices! Now starting detection loop!");
		
        ready();
        while (true) {
			// Run this code every <specified time>
			long currentTimeSeconds = (new Date()).getTime() / 1000;
			if ((currentTimeSeconds - lastTimeChecked) > CHECK_TIME_WAIT) {
				lastTimeChecked = currentTimeSeconds;
                
                System.out.println("loop begin");
				
                // Update the status of all devices
				updateUniversalStatus();

				// Decide to turn on alarm if any of the sensor/camera detects something unusual
				t1.update();
                // check if all alarm is enable or not.
                IoTString iall_alarm_val = t1.getCommitted(iall_alarm);
                if((iall_alarm_val != null) && (iall_alarm_val.equals(iable)==true)){ 
                    System.out.println("ALL ALARM : ABLED!");
                    decideToTurnOnAlarm();
                }else{
                    System.out.println("ALL ALARM : DISABLED!");
                    continue;
                }
                
                //Turn off the alarm when user press the button "turn off alarm"
                for(int zoneId=0; zoneId<ROOM_NUM; zoneId++){
				    IoTString ialarm_val = t1.getCommitted(ialarm_num[zoneId]);
                    if((ialarm_val != null)&&(ialarm_val.equals(ioff))){
                        turnOffAlarms(zoneId);
                    }
                }


				// Decide to turn off alarm if every sensor/camera goes back to normal
				//decideToTurnOffAlarm();

			} else {

				try {

					Thread.sleep(CHECK_TIME_WAIT * 100); // sleep for a tenth of the time

				} catch (Exception e) {

					e.printStackTrace();
				}
			}

		}
	}
}


